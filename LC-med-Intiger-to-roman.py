args = [25,
        1, 2, 3, 4, 5, 6, 7, 8, 9,
        58, 
        1994
        ]

def valueConverter(value):
    int(value)
    dictKey = []
    multiplier = []
    position = []
    if 1 <= value < 4:
        dictKey = [1]
        multiplier = [value]
        position = [0]
    elif value == 4:
        dictKey = [1, 2]
        multiplier = [1, 1]   
        position = [0, 0]
    elif value == 5:
        dictKey = [2]
        multiplier =[1]
        position = [0]
    elif 5 < value < 9:
        dictKey = [2, 1]
        multiplier = [1, value-5]
        position = [0, 0]
    elif value == 9:
        dictKey = [1, 1]
        multiplier = [1, 1]
        position = [0, 1]
    return dictKey, multiplier, position


def intToRoman(num):
    numAsList = list(str(num))
    numAsList.reverse()
    fullNumeral = ""
    outputAsList = []
    intDict = {
        1: ["I", "X", "C", "M"],
        2: ["V", "L", "D"],
        
    }
    for count, value in enumerate(numAsList, start = 1):
        placeAndValueConverter = valueConverter(int(value))
        dictKey  = placeAndValueConverter[0]
        multiplier = placeAndValueConverter[1]
        position = placeAndValueConverter[2]
        for i in range(len(dictKey)):
            numeralPart = intDict[dictKey[i]][position[i]+count-1]*multiplier[i]
            fullNumeral += numeralPart
        # if fullNumeral != None:
            outputAsList.append(fullNumeral)
            fullNumeral = ""
    corectList = outputAsList[::-1]
    output = "".join(corectList)
    return output

for arg in args:
    print(intToRoman(arg))