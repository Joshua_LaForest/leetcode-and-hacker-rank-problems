This is a remote repository for my leetcode/hacker rank problems so that
I can access past opperations easily for reference.

My naming convention is
LC == Leetcode or HR == Hacker-rank
e, med, h == Easy, Medium or Hard difficulty as applicable
And then the name of the problem.